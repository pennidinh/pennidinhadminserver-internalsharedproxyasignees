#!/usr/bin/env bash

touch buildArtifact.zip && rm buildArtifact.zip && zip -r buildArtifact.zip ./

aws s3  cp ./buildArtifact.zip  s3://pennidinh-code-assets/pennidinhadminserver-internalsharedproxyasignees.zip

aws lambda update-function-code --function-name PenniDinhCentral-InternalSharedProxyAsignees --s3-bucket pennidinh-code-assets --s3-key pennidinhadminserver-internalsharedproxyasignees.zip --region us-west-2
