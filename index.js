const safeJsonStringify = require('safe-json-stringify');
const AWS = require('aws-sdk');
// Set the Region
AWS.config.update({region: 'us-west-2'});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

exports.handler = function(event, context, callback) {
    console.log('Event: ' + safeJsonStringify(event));
    console.log('Handling lambda invocation...');

    const sharedProxyDomainName = event['sharedProxyDomainName'];
    if (!sharedProxyDomainName) {
      callback(Error('sharedProxyDomainName parameter must be present!'));
      return;
    }
    const service = event['service'];
    if (service != 'https' && service != 'ssh') {
      callback(Error('service param must equal https or ssh!'));
      return;
    }
    
    const startKey = event['startKey'] ? JSON.parse(event['startKey']) : null;
    ddb.query({ExclusiveStartKey: startKey, TableName: process.env.SharedProxyAssignmentsDynamoDBTableName, IndexName: 'hostname', KeyConditionExpression: 'hostname = :hostnameVal', ExpressionAttributeValues: {':service': {'S': service}, ':hostnameVal': {'S': sharedProxyDomainName}}, FilterExpression: 'service = :service', Limit: 25}, function (err, data) {
      if (err) {
        console.log("Error", err);
        callback(Error(err));
        return;
      }

      const nextStartKey = safeJsonStringify(data.LastEvaluatedKey);
      if (data.Items.length == 0) {
        callback(null, {nextStartKey: nextStartKey, results: []});
        return;
      }

      const externalSubdomains = data.Items.map(item => item['externalSubdomain'].S);
      const externalSubdomainToPort = {};
      for (var i = 0; i < data.Items.length; i++) {
        const item = data.Items[i];
        externalSubdomainToPort[item['externalSubdomain'].S] = item['port'].N
      }
      const reqItems = {}
      reqItems[process.env.SubdomainAssignmentsDynamoDBTableName] = {Keys: externalSubdomains.map(function(externalSubdomain) { return { 'Subdomain': {'S': externalSubdomain} } })};
      ddb.batchGetItem(
        {RequestItems: reqItems},
        function (err, data) {
          if (err) {
            console.log("Error", err);
            callback(err);
            return;
          }

          if (!data.Responses[process.env.SubdomainAssignmentsDynamoDBTableName] || data.Responses[process.env.SubdomainAssignmentsDynamoDBTableName].length == 0) {
            callback(null, {nextStartKey: nextStartKey, results: []});
            return;
          }

          const clientIds = data.Responses[process.env.SubdomainAssignmentsDynamoDBTableName].map(item => item['ClientId'].S);
          const externalSubdomainToClientId = {};
          for (var i = 0; i < data.Responses[process.env.SubdomainAssignmentsDynamoDBTableName].length; i++) {
            const item = data.Responses[process.env.SubdomainAssignmentsDynamoDBTableName][i];
            externalSubdomainToClientId[item['Subdomain'].S] = item['ClientId'].S
          }
          const requestItems = {};
          requestItems[process.env.ClientSshPublicKeysTableName] = {Keys: clientIds.map(function(clientId) { return {'clientId': {'S' : clientId} } })};
          ddb.batchGetItem({RequestItems: requestItems}, function (err, data) {
            if (err) {
              console.log("Error", err);
              callback(err);
              return;
            }

            if (!data.Responses[process.env.ClientSshPublicKeysTableName] || data.Responses[process.env.ClientSshPublicKeysTableName].length == 0) {
              callback(null, {nextStartKey: nextStartKey, results: []});
              return;
            }

            const externalClientIdToSshPublicKey = {};
            for (var i = 0; i < data.Responses[process.env.ClientSshPublicKeysTableName].length; i++) {
              const item = data.Responses[process.env.ClientSshPublicKeysTableName][i];
              externalClientIdToSshPublicKey[item['clientId'].S] = item['sshPubKey'].S
            }


            const results = [];
            for (var i = 0; i < externalSubdomains.length; i++) {
              const externalSubdomain = externalSubdomains[i];
              results.push({externalSubdomain: externalSubdomain, port: externalSubdomainToPort[externalSubdomain], clientId: externalSubdomainToClientId[externalSubdomain], sshPubKey: externalClientIdToSshPublicKey[externalSubdomainToClientId[externalSubdomain]]});
            }
            callback(null, {nextStartKey: nextStartKey, results: results});
          })
        }
      )
    }) 
}
